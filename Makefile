test:
	go test -v ./...
run:
	go run main.go -v
fix-lint:
	golangci-lint run -c .golangci.yml -v --fix
lint:
	golangci-lint run -c .golangci.yml -v
build-docker:
	docker build --tag ys-golang .