package domain

import "errors"

type KeyStore struct {
	ID    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

func (k *KeyStore) IsValid() []error {
	var errs []error
	if k.Key == "" {
		errs = append(errs, errors.New("the key is required"))
	}
	if k.Value == "" {
		errs = append(errs, errors.New("the value is required"))
	}
	return errs
}
