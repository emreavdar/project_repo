package domain

type KeyStoreResponse struct {
	Error string    `json:"error"`
	Data  *KeyStore `json:"data"`
}
