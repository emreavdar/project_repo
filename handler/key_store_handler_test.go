package handler_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/emrecavdar94/ys-golang/domain"
	"github.com/emrecavdar94/ys-golang/handler"
	"github.com/emrecavdar94/ys-golang/repository"
	"github.com/emrecavdar94/ys-golang/service"
	"github.com/gavv/httpexpect/v2"
)

func TestKeyStoreHandler_CreateNewKeyStore(t *testing.T) {
	repo := repository.NewKeyStoreRepository([]*domain.KeyStore{})
	serv := service.NewKeyStoreService(repo)
	handle := handler.NewKeyStoreHandler(serv)
	dataToBeInserted := &domain.KeyStore{
		Key:   "testKey",
		Value: "testValue",
	}
	t.Run("Success", func(t *testing.T) {
		server := httptest.NewServer(handle)
		defer server.Close()

		e := httpexpect.New(t, server.URL)
		e.POST("/api/v1/key").WithJSON(dataToBeInserted).
			Expect().
			Status(http.StatusCreated).
			JSON().Object().ContainsKey("error").ValueEqual("error", "")
	})

	t.Run("Error", func(t *testing.T) {
		repo := repository.NewKeyStoreRepository([]*domain.KeyStore{})
		serv := service.NewKeyStoreService(repo)
		handle = handler.NewKeyStoreHandler(serv)
		server := httptest.NewServer(handle)
		defer server.Close()
		e := httpexpect.New(t, server.URL)
		e.POST("/api/v1/key").WithJSON(dataToBeInserted).
			Expect().
			Status(http.StatusCreated).
			JSON().Object().ContainsKey("error").ValueEqual("error", "")

		e.POST("/api/v1/key").WithJSON(dataToBeInserted).
			Expect().
			Status(http.StatusConflict).
			JSON().Object().ContainsKey("error").ValueEqual("error", "keystore already exists")
	})
}

func TestKeyStoreHandler_GetKeyStoreByKey(t *testing.T) {
	repo := repository.NewKeyStoreRepository([]*domain.KeyStore{{ID: 1, Key: "testKey", Value: "testVal"}})
	serv := service.NewKeyStoreService(repo)
	handle := handler.NewKeyStoreHandler(serv)

	t.Run("Success", func(t *testing.T) {
		server := httptest.NewServer(handle)
		defer server.Close()

		e := httpexpect.New(t, server.URL)
		e.GET("/api/v1/key/testKey").
			Expect().
			Status(http.StatusOK).
			JSON().Object().ContainsKey("data").ValueEqual("data", &domain.KeyStore{ID: 1, Key: "testKey", Value: "testVal"}).
			ContainsKey("error").ValueEqual("error", "")
	})
	t.Run("Not Found", func(t *testing.T) {
		server := httptest.NewServer(handle)
		defer server.Close()

		e := httpexpect.New(t, server.URL)
		e.GET("/api/v1/key/testKey2").
			Expect().
			Status(http.StatusNotFound).
			JSON().Object().ContainsKey("data").ValueEqual("data", nil).
			ContainsKey("error").ValueEqual("error", "keystore not found")
	})
}
