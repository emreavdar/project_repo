package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/emrecavdar94/ys-golang/logger"

	"github.com/emrecavdar94/ys-golang/domain"
	"github.com/emrecavdar94/ys-golang/errors"
	"github.com/emrecavdar94/ys-golang/service"
)

type KeyStoreHandler interface {
	GetKeyStoreByKey(w http.ResponseWriter, r *http.Request) error
	CreateNewKeyStore(w http.ResponseWriter, r *http.Request) error
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}
type keyStoreHandler struct {
	kss service.KeyStoreService
}

func NewKeyStoreHandler(kss service.KeyStoreService) KeyStoreHandler {
	return &keyStoreHandler{
		kss: kss,
	}
}

func (ksh *keyStoreHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	if r.URL.Path == "/api/v1/key" && r.Method == http.MethodPost {
		_ = ksh.CreateNewKeyStore(w, r)
	} else if regexp.MustCompile(`/api/v1/key/[a-zA-Z]+`).MatchString(r.URL.Path) && r.Method == http.MethodGet {
		_ = ksh.GetKeyStoreByKey(w, r)
	} else {
		http.Error(w, fmt.Sprintf("expect method GET, DELETE or POST at /key/, got %v", r.Method), http.StatusMethodNotAllowed)
		return
	}
}

func (ksh *keyStoreHandler) GetKeyStoreByKey(w http.ResponseWriter, r *http.Request) error {
	status := http.StatusOK
	id := strings.TrimPrefix(r.URL.Path, "/api/v1/key/")
	logger.LogErrToFile(fmt.Sprintf("rest request for getting key store data with key id %s, path: %s, ip: %s",
		id, r.URL.Path, r.RemoteAddr), "info")

	keystore, err := ksh.kss.GetKeyStoreByKey(id)
	var response domain.KeyStoreResponse
	if err != nil {
		if err != errors.ErrKeyStoreNotFound {
			status = http.StatusInternalServerError
			response = domain.KeyStoreResponse{
				Error: errors.ErrInternalError.Error(),
				Data:  nil,
			}
		} else {
			status = http.StatusNotFound
			response = domain.KeyStoreResponse{
				Error: errors.ErrKeyStoreNotFound.Error(),
				Data:  nil,
			}
		}
	} else {
		response = domain.KeyStoreResponse{
			Error: "",
			Data:  keystore,
		}
	}
	w.WriteHeader(status)
	logger.LogErrToFile(fmt.Sprintf(
		"rest response for getting key store data with key id %s, status: %s, data: %v,  path: %s, ip: %s",
		id, strconv.Itoa(status), response, r.URL.Path, r.RemoteAddr), "info")
	return json.NewEncoder(w).Encode(response)
}

func (ksh *keyStoreHandler) CreateNewKeyStore(w http.ResponseWriter, r *http.Request) error {
	status := http.StatusCreated
	reqBody, _ := ioutil.ReadAll(r.Body)
	logger.LogErrToFile(fmt.Sprintf(
		"rest request to create key store data with request body %s, path: %s, ip: %s",
		reqBody, r.URL.Path, r.RemoteAddr), "info")

	var keystoreData domain.KeyStore
	var response domain.KeyStoreResponse
	err := json.Unmarshal(reqBody, &keystoreData)
	if err != nil {
		return err
	}
	err = ksh.kss.CreateNewKeyStore(&keystoreData)
	if err != nil {
		if err != errors.ErrKeyStoreAlreadyExists {
			status = http.StatusInternalServerError
		} else {
			status = http.StatusConflict
		}
		response = domain.KeyStoreResponse{
			Error: err.Error(),
			Data:  nil,
		}
	} else {
		response = domain.KeyStoreResponse{Error: "", Data: nil}
	}
	w.WriteHeader(status)
	logger.LogErrToFile(fmt.Sprintf(
		"rest response to create key store data with request body %s, status: %s, data: %v,  path: %s, ip: %s",
		reqBody, strconv.Itoa(status), response, r.URL.Path, r.RemoteAddr), "info")
	return json.NewEncoder(w).Encode(response)
}
