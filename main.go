package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/emrecavdar94/ys-golang/domain"
	"github.com/emrecavdar94/ys-golang/handler"
	"github.com/emrecavdar94/ys-golang/repository"
	"github.com/emrecavdar94/ys-golang/service"
)

func main() {
	keyStoreRepository := repository.NewKeyStoreRepository([]*domain.KeyStore{})
	keyStoreService := service.NewKeyStoreService(keyStoreRepository)
	keyStoreHandler := handler.NewKeyStoreHandler(keyStoreService)
	keyStoreService.InitializeKeyStoreData()
	go keyStoreService.WriteKeyStoreToFile()
	mux := http.NewServeMux()
	mux.Handle("/api/v1/key", keyStoreHandler)
	mux.Handle("/api/v1/key/", keyStoreHandler)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	err := http.ListenAndServe(fmt.Sprintf(":%s", port), mux)
	if err != nil {
		log.Fatalf("An error occurred while running the application")
	}
}
