package errors

import "errors"

var (
	ErrKeyStoreAlreadyExists = errors.New("keystore already exists")
	ErrKeyStoreNotFound      = errors.New("keystore not found")
	ErrInternalError         = errors.New("facing a error")
)
