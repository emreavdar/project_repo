package repository

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/emrecavdar94/ys-golang/domain"
)

type KeyStoreRepository interface {
	CreateNewKeyStore(keystore *domain.KeyStore) error
	GetKeyStoreByKey(key string) (*domain.KeyStore, error)
	WriteKeyStoreToFile() error
	InitializeKeyStoreData(file string) error
}

type keyStoreRepository struct {
	keystore []*domain.KeyStore
}

func NewKeyStoreRepository(keystore []*domain.KeyStore) KeyStoreRepository {
	return &keyStoreRepository{keystore: keystore}
}

func (ksr *keyStoreRepository) CreateNewKeyStore(keystore *domain.KeyStore) error {
	if len(ksr.keystore) > 0 {
		keystore.ID = ksr.keystore[len(ksr.keystore)-1].ID + 1
	}
	appendedData := ksr.addData(keystore)
	if appendedData == nil {
		return errors.New("key not be created")
	}
	ksr.keystore = appendedData
	return nil
}

func (ksr *keyStoreRepository) addData(keystore *domain.KeyStore) []*domain.KeyStore {
	return append(ksr.keystore, keystore)
}

func (ksr *keyStoreRepository) GetKeyStoreByKey(key string) (*domain.KeyStore, error) {
	for _, v := range ksr.keystore {
		if v.Key == key {
			return v, nil
		}
	}
	return &domain.KeyStore{}, errors.New("key not found")
}

func (ksr *keyStoreRepository) WriteKeyStoreToFile() error {
	var stringBuilder strings.Builder
	var base = 10
	if _, err := os.Stat("tmp/"); os.IsNotExist(err) {
		err := os.Mkdir("tmp/", os.ModePerm)
		if err != nil {
			log.Println(err.Error())
		}
	}
	stringBuilder.WriteString("tmp/")
	stringBuilder.WriteString(strconv.FormatInt(time.Now().Unix(), base))
	stringBuilder.WriteString("-data.json")
	f, err := os.Create(stringBuilder.String())
	if err != nil {
		return err
	}
	log.Printf("'%s' file created", f.Name())

	defer f.Close()
	j, err := json.Marshal(ksr.keystore)
	if err != nil {
		return err
	}
	_, err = f.Write(j)
	if err != nil {
		log.Printf("f.write failed with '%s'\n", err)
		return err
	}
	log.Printf("'%s' data writed to '%s' file ", string(j), f.Name())

	return nil
}

func (ksr *keyStoreRepository) InitializeKeyStoreData(file string) error {
	jsonFile, err := os.Open(file)
	if err != nil {
		log.Fatalf("os.Open failed with '%s'\n", err)
	}
	fmt.Printf("Successfully Opened '%s'", file)
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	err = json.Unmarshal(byteValue, &ksr.keystore)
	if err != nil {
		return err
	}

	fmt.Println(ksr.keystore)

	return nil
}
