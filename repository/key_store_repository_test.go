package repository_test

import (
	"testing"

	"github.com/emrecavdar94/ys-golang/domain"
	"github.com/emrecavdar94/ys-golang/repository"
)

func TestKeyStoreRepository_CreateNewKeyStore(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		repo := repository.NewKeyStoreRepository([]*domain.KeyStore{})
		dataToBeInserted := &domain.KeyStore{
			Key:   "testKey",
			Value: "testValue",
		}
		err := repo.CreateNewKeyStore(dataToBeInserted)
		if err != nil {
			t.Errorf("Creating new keystore test failing with '%s'", err.Error())
		}
	})
}

func TestKeyStoreRepository_GetKeyStoreByKey(t *testing.T) {
	expectedVal := &domain.KeyStore{
		ID:    1,
		Key:   "testKey",
		Value: "testVal",
	}
	repo := repository.NewKeyStoreRepository([]*domain.KeyStore{{
		ID:    1,
		Key:   "testKey",
		Value: "testVal",
	}})
	t.Run("Success", func(t *testing.T) {
		data, err := repo.GetKeyStoreByKey("testKey")
		if err != nil {
			t.Errorf("Getting keystore test failing with '%s'", err.Error())
		}
		if data == nil {
			t.Errorf("Getting new keystore test failing with '%s'", err.Error())
		} else if (data != nil) && (data.Key != expectedVal.Key || data.Value != expectedVal.Value || data.ID != expectedVal.ID) {
			t.Errorf("expected: %q but actual: %q", expectedVal, data)
		}
	})
}

func TestKeyStoreRepository_WriteKeyStoreToFile(t *testing.T) {
	repo := repository.NewKeyStoreRepository([]*domain.KeyStore{{
		ID:    1,
		Key:   "testKey",
		Value: "testVal",
	}})
	t.Run("Success", func(t *testing.T) {
		err := repo.WriteKeyStoreToFile()
		if err != nil {
			t.Errorf("Writing keystore data to file failing with '%s'", err.Error())
		}
	})
}
