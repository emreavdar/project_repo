package service

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"github.com/emrecavdar94/ys-golang/domain"
	customError "github.com/emrecavdar94/ys-golang/errors"
	"github.com/emrecavdar94/ys-golang/repository"
)

type KeyStoreService interface {
	CreateNewKeyStore(keystore *domain.KeyStore) error
	GetKeyStoreByKey(key string) (*domain.KeyStore, error)
	WriteKeyStoreToFile()
	InitializeKeyStoreData()
}

type keyStoreService struct {
	ksr repository.KeyStoreRepository
}

func NewKeyStoreService(ksr repository.KeyStoreRepository) KeyStoreService {
	return &keyStoreService{
		ksr: ksr,
	}
}

func (kss *keyStoreService) CreateNewKeyStore(keystore *domain.KeyStore) error {
	errs := keystore.IsValid()
	var errBuilder strings.Builder
	if len(errs) > 0 {
		for _, val := range errs {
			errBuilder.WriteString(val.Error())
			errBuilder.WriteString(" ")
		}
		return errors.New(errBuilder.String())
	}
	data, _ := kss.GetKeyStoreByKey(keystore.Key)
	if data != nil {
		return customError.ErrKeyStoreAlreadyExists
	}
	err := kss.ksr.CreateNewKeyStore(keystore)
	if err != nil {
		return err
	}
	return nil
}

func (kss *keyStoreService) GetKeyStoreByKey(key string) (*domain.KeyStore, error) {
	keyStore, err := kss.ksr.GetKeyStoreByKey(key)
	if err != nil {
		return nil, customError.ErrKeyStoreNotFound
	}
	return keyStore, nil
}
func (kss *keyStoreService) WriteKeyStoreToFile() {
	ticker := time.NewTicker(1 * time.Minute)
	for range ticker.C {
		err := kss.ksr.WriteKeyStoreToFile()
		if err != nil {
			log.Println(err.Error())
		}
	}
}

func (kss *keyStoreService) InitializeKeyStoreData() {
	dir := "tmp/"
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.Mkdir(dir, os.ModePerm)
		if err != nil {
			log.Println(err.Error())
		}
	}
	files, _ := ioutil.ReadDir(dir)
	var newestFile string
	var filePath strings.Builder
	var newestTime int64
	if len(files) > 0 {
		for _, f := range files {
			fi, err := os.Stat(dir + f.Name())
			if err != nil {
				fmt.Println(err)
			}
			currTime := fi.ModTime().Unix()
			if currTime > newestTime {
				newestTime = currTime
				newestFile = f.Name()
			}
		}
		filePath.WriteString("tmp/")
		filePath.WriteString(newestFile)
		err := kss.ksr.InitializeKeyStoreData(filePath.String())
		if err != nil {
			log.Println(err.Error())
		}
	}
}
