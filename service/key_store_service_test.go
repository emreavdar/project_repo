package service_test

import (
	"testing"

	"github.com/emrecavdar94/ys-golang/domain"
	"github.com/emrecavdar94/ys-golang/errors"
	"github.com/emrecavdar94/ys-golang/repository"
	"github.com/emrecavdar94/ys-golang/service"
)

func TestKeyStoreService_CreateNewKeyStore(t *testing.T) {
	repo := repository.NewKeyStoreRepository([]*domain.KeyStore{})
	serv := service.NewKeyStoreService(repo)
	t.Run("Success", func(t *testing.T) {
		dataToBeInserted := &domain.KeyStore{
			Key:   "testKey",
			Value: "testValue",
		}
		err := serv.CreateNewKeyStore(dataToBeInserted)
		if err != nil {
			t.Errorf("Creating new keystore test failing with '%s'", err.Error())
		}
	})
	t.Run("Already Exist", func(t *testing.T) {
		dataToBeInserted := &domain.KeyStore{
			Key:   "testKey",
			Value: "testValue",
		}
		_ = serv.CreateNewKeyStore(dataToBeInserted)
		err := serv.CreateNewKeyStore(dataToBeInserted)
		if err != errors.ErrKeyStoreAlreadyExists {
			t.Errorf("Existing item should not be added")
		}
	})

	t.Run("Validation Error", func(t *testing.T) {
		dataToBeInserted := &domain.KeyStore{
			Key:   "",
			Value: "testValue",
		}
		err := serv.CreateNewKeyStore(dataToBeInserted)
		if err == nil {
			t.Error("Data should not be added if there is a validation error")
		}
	})
}

func TestKeyStoreService_GetKeyStoreByKey(t *testing.T) {
	expectedVal := &domain.KeyStore{
		ID:    1,
		Key:   "testKey",
		Value: "testVal",
	}
	repo := repository.NewKeyStoreRepository([]*domain.KeyStore{{
		ID:    1,
		Key:   "testKey",
		Value: "testVal",
	}})
	serv := service.NewKeyStoreService(repo)
	t.Run("Success", func(t *testing.T) {
		data, err := serv.GetKeyStoreByKey("testKey")
		if err != nil {
			t.Errorf("Getting keystore test failing with '%s'", err.Error())
		}
		if data == nil {
			t.Errorf("Getting new keystore test failing with '%s'", err.Error())
		} else if (data != nil) && (data.Key != expectedVal.Key || data.Value != expectedVal.Value || data.ID != expectedVal.ID) {
			t.Errorf("expected: %q but actual: %q", expectedVal, data)
		}
	})
	t.Run("Not Found", func(t *testing.T) {
		_, err := serv.GetKeyStoreByKey("testKey2")
		if err != errors.ErrKeyStoreNotFound {
			t.Errorf("GetKeyStoreByKey method should be throw not found error")
		}
	})
}

func TestKeyStoreService_InitializeKeyStoreData(t *testing.T) {
	repo := repository.NewKeyStoreRepository([]*domain.KeyStore{{
		ID:    1,
		Key:   "testKey",
		Value: "testVal",
	}})
	_ = repo.WriteKeyStoreToFile()
	repo = repository.NewKeyStoreRepository([]*domain.KeyStore{})
	serv := service.NewKeyStoreService(repo)
	serv.InitializeKeyStoreData()

	_, err := repo.GetKeyStoreByKey("testKey")

	if err != nil {
		t.Errorf("Initializing data from file test failing with %s", err)
	}
}
