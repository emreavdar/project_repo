# Yemeksepeti Golang Project

## About the project

The project is used to store key-value data.Key-value datas is saved to TIMESTAMP-data.json file every hour
and when app build if there are any data, data is loaded.


### API docs
| Route | Method     | Description     | Request body
| --- | --- | --- | --- |
| /api/v1/key/:key | `GET` |Get specific key-store data with key  |  |
| /api/v1/key | `POST` | Create a key-store data.  | {"key":"keyData","value":"valueData"} |


## Installation
First you need to have `go` installed, if you don't have it yet.
https://golang.org/doc/install

Then, To install dependencies.
```bash
go mod download
```

## Usage
#### This project deployed to heroku. Link: https://ys-golang-prod.herokuapp.com/

Run this project
```bash
make run
```
Run tests
```bash
make test
```
Fix lint errors
```bash
make fix-lint
```
Lint
```bash
make fix-lint
```

### Layout

```tree
├── .gitignore
├── .golangci.yml
├── .gitlab-ci.yml
├── Makefile
├── Dockerfile
├── LICENSE
├── README.md
├── domain
│   ├── key_store.go
│   └── key_store_response.go
├── errors
│   └── key_store.go
├── handler
│   └── key_store_handler.go
├── handler
│   └── http logs
├── logger
│   └── logger.go
├── repository
│   ├── temp
│   │   └── log files for test
│   ├── key_store_repository.go
│   └── key_store_repository_test.go
├── service
│   ├── temp
│   │   └── log files for test
│   ├── key_store_service.go
│   └── key_store_service_test.go
```