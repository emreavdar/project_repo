package logger

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"
)

func LogErrToFile(message, errType string) {
	var stringBuilder strings.Builder
	var file *os.File
	file, _ = os.OpenFile("log/server-log.txt", os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if file == nil {
		file, _ = os.Create("log/server-log.txt")
	}

	stringBuilder.WriteString(fmt.Sprintf("[%s]-[%s] -> %s\n", time.Now(), errType, message))
	defer file.Close()
	_, err := file.WriteString(stringBuilder.String())
	if err != nil {
		log.Printf("logging failed with '%s'\n", err)
	}
}
